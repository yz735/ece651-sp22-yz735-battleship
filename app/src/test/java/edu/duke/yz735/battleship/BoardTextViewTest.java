package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {

  /* this tests the display of an empty 2 * 2 Board
   */
  @Test
  public void test_display_empty_2by2() {
    emptyBoardHelper(2,2, "  0|1\n", "A  |  A\nB  |  B\n");
  }
  /* this tests the display of an empty 3 * 2 Board
   */
  @Test
  public void test_display_empty_3by2() {
    emptyBoardHelper(3,2, "  0|1|2\n", "A  | |  A\nB  | |  B\n");
  }
  /* this tests the display of an empty 3 * 5 Board
   */
  @Test
  public void test_display_empty_3by5() {
    emptyBoardHelper(3,5, "  0|1|2\n", "A  | |  A\nB  | |  B\nC  | |  C\nD  | |  D\nE  | |  E\n");
  }

  /**
   * this abstract out the test for different sized empty boards
   * @param w: width of the board
   * @param h: height of the board
   * @param expectedHeader: Header Text output in String 
   * @param expectedBody: Body Text output in String
   */
  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
    Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  /* this tests the display of a non-empty 3 * 4 Board
   */
  @Test
  public void test_display_nonempty_4by3() {
    Board<Character> b = new BattleShipBoard<Character>(4, 3, 'X');
    BoardTextView view = new BoardTextView(b);
    V1ShipFactory f = new V2ShipFactory();
    Ship<Character> s1 = f.makeSubmarine(new Placement("B0H"));
    Ship<Character> s2 = f.makeDestroyer(new Placement("A3V"));

    b.tryAddShip(s1);
    b.tryAddShip(s2);
    b.fireAt(new Coordinate("B1"));//hit submarine
    b.fireAt(new Coordinate("C2"));//miss
    b.fireAt(new Coordinate("A3"));//hit destroyer
    
    String myView =
      "  0|1|2|3\n" +
      "A  | | |* A\n" +
      "B s|*| |d B\n" +
      "C  | | |d C\n" +
      "  0|1|2|3\n";
    //make sure we laid things out the way we think we did.
    assertEquals(myView, view.displayMyOwnBoard());
    
    String enemyView = 
      "  0|1|2|3\n" +
      "A  | | |d A\n" +
      "B  |s| |  B\n" +
      "C  | |X|  C\n" +
      "  0|1|2|3\n";
    //make sure we laid things out the way we think we did.
    assertEquals(enemyView, view.displayEnemyBoard());
  }

  /* this tests the display of a non-empty 3 * 4 Board
   */
  @Test
  public void test_display_side_by_side_nonempty_4by3() {
    Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
    Board<Character> b2 = new BattleShipBoard<Character>(4, 3, 'X');
    
    BoardTextView myView = new BoardTextView(b1);
    BoardTextView enemyView = new BoardTextView(b2);
    
    V1ShipFactory f = new V2ShipFactory();
    Ship<Character> s1_1 = f.makeSubmarine(new Placement("B0H"));
    Ship<Character> s1_2 = f.makeDestroyer(new Placement("A3V"));

    Ship<Character> s2_1 = f.makeSubmarine(new Placement("A0V"));
    Ship<Character> s2_2 = f.makeDestroyer(new Placement("C1H"));

    b1.tryAddShip(s1_1);
    b1.tryAddShip(s1_2);
    b1.fireAt(new Coordinate("B1"));//hit submarine
    b1.fireAt(new Coordinate("C2"));//miss
    b1.fireAt(new Coordinate("A3"));//hit destroyer

    b2.tryAddShip(s2_1);
    b2.tryAddShip(s2_2);
    b2.fireAt(new Coordinate("B0"));//hit submarine
    b2.fireAt(new Coordinate("C2"));//miss
    b2.fireAt(new Coordinate("A3"));//hit destroyer

    String sideBySideDisplay =
      "     Your Ocean               Player B's Ocean\n" +
      "  0|1|2|3                    0|1|2|3\n" +
      "A  | | |* A                A  | | |X A\n" +
      "B s|*| |d B                B s| | |  B\n" +
      "C  | | |d C                C  | |d|  C\n" +
      "  0|1|2|3                    0|1|2|3\n";
      //make sure we laid things out the way we think we did.
    assertEquals(sideBySideDisplay, myView.displayMyBoardWithEnemyNextToIt(enemyView, "Your Ocean",  "Player B's Ocean"));
  }
  
  /* this tests the constructor will throw IllegalArgumentException
   * if input dimensions for the board is larger than 10*26
   */
  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

}
