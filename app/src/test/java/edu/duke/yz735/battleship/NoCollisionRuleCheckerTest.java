package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_no_collision() {
    PlacementRuleChecker<Character> checker = new NoCollisionRuleChecker<Character>(null);
    Board<Character> b1 = new BattleShipBoard<Character>(5, 10, 'X');
    V1ShipFactory shipFactory = new V2ShipFactory();
    Ship<Character> s1  = shipFactory.makeDestroyer(new Placement("A0H"));
    Ship<Character> s2  = shipFactory.makeDestroyer(new Placement("A0V"));

    assertEquals(checker.checkPlacement(s1,b1),null);
    b1.tryAddShip(s1);
    assertEquals(checker.checkPlacement(s2,b1),"That placement is invalid: the ship overlaps another ship.");
  }

  @Test
  public void test_inBound_and_noCollision() {
    PlacementRuleChecker<Character> NoCollisionChecker = new NoCollisionRuleChecker<Character>(null);
    PlacementRuleChecker<Character> InBoundChecker = new InBoundsRuleChecker<Character>(NoCollisionChecker);
    Board<Character> b1 = new BattleShipBoard<Character>(5, 10, 'X');
    V1ShipFactory shipFactory = new V2ShipFactory();
    Ship<Character> s1  = shipFactory.makeDestroyer(new Placement("A5H"));
    Ship<Character> s2  = shipFactory.makeDestroyer(new Placement("A0V"));
    Ship<Character> s3  = shipFactory.makeSubmarine(new Placement("A0H"));
    
    assertEquals(InBoundChecker.checkPlacement(s1,b1), "That placement is invalid: the ship goes off the right of the board.");
    assertEquals(InBoundChecker.checkPlacement(s2,b1), null);
    b1.tryAddShip(s2);
    assertEquals(InBoundChecker.checkPlacement(s3,b1), "That placement is invalid: the ship overlaps another ship.");
  }
}
