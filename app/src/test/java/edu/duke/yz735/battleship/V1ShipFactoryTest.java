package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
  
   private void checkShip(Ship<Character> testShip, String expectedName,
                          char expectedLetter, Coordinate... expectedLocs){
     assertEquals(testShip.getName(), expectedName);
     for (Coordinate c : expectedLocs){
       assertEquals(testShip.getDisplayInfoAt(c,true), expectedLetter);
     }
   }
  
  
  @Test
  public void test_ShipFactory() {
    /** Test the methods in V1ShipFactory
     */
    V1ShipFactory f = new V1ShipFactory();

    //test valid ships
    Placement w1 = new Placement("A0V");

    Ship<Character> sub = f.makeSubmarine(w1);
    checkShip(sub, "Submarine", 's', new Coordinate("A0"), new Coordinate("B0"));
    assertFalse(sub.occupiesCoordinates(new Coordinate("C0")));
    
    Ship<Character> car = f.makeCarrier(w1);
    checkShip(car, "Carrier", 'c', new Coordinate("A0"), new Coordinate("B0"), new Coordinate("C0"), new Coordinate("D0"), new Coordinate("E0"), new Coordinate("F0"));
    assertFalse(car.occupiesCoordinates(new Coordinate("G0")));
    
    Placement w2 = new Placement("B3H");

    Ship<Character> bat = f.makeBattleship(w2);
    checkShip(bat, "Battleship", 'b', new Coordinate("B3"), new Coordinate("B4"), new Coordinate("B5"), new Coordinate("B6"));
    assertFalse(bat.occupiesCoordinates(new Coordinate("C3")));
    
    Ship<Character> dst = f.makeDestroyer(w2);
    checkShip(dst, "Destroyer", 'd', new Coordinate("B3"), new Coordinate("B4"), new Coordinate("B5"));
    assertFalse(dst.occupiesCoordinates(new Coordinate("C3")));

    //test invalid ships
    Placement wU = new Placement("A0U");
    Placement wR = new Placement("A0R");
    Placement wD = new Placement("A0D");
    Placement wL = new Placement("A0L");

    assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(wU));
    assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(wR));
    assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(wD));
    assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(wL));
  }

}
