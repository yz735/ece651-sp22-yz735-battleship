package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  //-----------TESTS-----------------------------------
  /** Test the function readPlacement
   */
  @Test
  public void test_read_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer validPlayer = createTextPlayer(10,20,"B2V\nC6H\na4v\n", bytes);
    TextPlayer nullPlayer = createTextPlayer(10,20,"", bytes);
    
    String prompt = "Please enter a location for a ship:";
    //tests the EOF error
    assertThrows(EOFException.class, () -> nullPlayer.readPlacement(prompt));
    bytes.reset();

    //tests for valid inputs
    ArrayList<Placement> expected = buildExpectedPlacements("B2V\nC6H\na4v\n".split("\n"));
    for (Placement pi : expected) {
      Placement p = validPlayer.readPlacement(prompt);
      assertEquals(p, pi); //did we get the right Placement back
      assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
      bytes.reset(); //clear out bytes for next time around
    }
    
    //tests for invalid inputs
    TextPlayer invalidPlayer = createTextPlayer(10,20,"A0Q\n", bytes);
    assertThrows(IllegalArgumentException.class, () -> invalidPlayer.readPlacement(prompt));
    bytes.reset();
  }

  /* Test the function doOnePlacement
   */
  @Test
  public void test_doOnePlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();

    //test for valid doOnePlacement
    TextPlayer p1 = createTextPlayer(10,20,"B2V\nC6H\na4R\n", bytes);
    ArrayList<Placement> expected = buildExpectedPlacements("B2V\nC6H\na4R\n".split(System.lineSeparator()));
    String expectedOutput = buildExpectedOutputs(expected, p1.shipsToPlace, p1.shipCreationFns);
    for (int i = 0; i < expected.size(); i++) {
      p1.doOnePlacement(p1.shipsToPlace.get(i), p1.shipCreationFns.get(p1.shipsToPlace.get(i)));
    }
    assertEquals(expectedOutput, bytes.toString()); //should have printed prompt and newline
    bytes.reset(); //clear out bytes for next time around
    
    //test for doOnePlacement that fails tryAddShip
    TextPlayer p2 = createTextPlayer(10,20,"Z0V\n", bytes);
    assertThrows(EOFException.class, () -> p2.doOnePlacement(p2.shipsToPlace.get(0), p2.shipCreationFns.get(p2.shipsToPlace.get(0))));
    assertEquals("Player A where do you want to place a " + p2.shipsToPlace.get(0) + "?\n" + "That placement is invalid: the ship goes off the bottom of the board.\n" + "Player A where do you want to place a " + p2.shipsToPlace.get(0) + "?\n" , bytes.toString());
    bytes.reset();

    //test for doOnePlacement that fails readPlacement
    TextPlayer p3 = createTextPlayer(10,20,"AQAdashasifang\n", bytes);
    assertThrows(EOFException.class, () -> p3.doOnePlacement(p3.shipsToPlace.get(0), p3.shipCreationFns.get(p3.shipsToPlace.get(0))));
    assertEquals("Player A where do you want to place a " + p3.shipsToPlace.get(0) + "?\n" + "That placement is invalid: it does not have the correct format.\n" + "Player A where do you want to place a " + p3.shipsToPlace.get(0) + "?\n" , bytes.toString());
    bytes.reset();
  }

  
  /** Test computer add ships random action
   */
  @Test
  public void test_computerPlacement() throws IOException{
    for (int i = 0; i<10; i++){
      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      TextPlayer p1 = createTextPlayer(10,20, "C\n", bytes);//player
      p1.chooseToBeHuman();
      p1.doPlacementPhase();
      System.out.println(p1.view.displayMyOwnBoard());
    }
  }
  
  /** Test the function readCoordinate
   */
  @Test
  public void test_read_coordinate() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = createTextPlayer(10,20,"A3\nB1v\nz0\n", bytes);
    TextPlayer p2 = createTextPlayer(10,20,"", bytes);

    assertEquals(new Coordinate("a3"), p1.readCoordinate(null));
    assertThrows(IllegalArgumentException.class, () -> p1.readCoordinate(null));
    assertThrows(IllegalArgumentException.class, () -> p1.readCoordinate(null));
    assertThrows(EOFException.class, ()->p2.readCoordinate(null));
  }

  /** Test the function actionFire
   */
  @Test
  public void test_readAction() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = attackingPhasePlayer(2,2,"p1","F\nm\nS\n", bytes);//player
    TextPlayer p2 = attackingPhasePlayer(2,2,"p2","F\nm\nS\n", bytes);//player

    char act1 = p2.readAction(p1);
    assertEquals(act1, 'F');
    assertEquals("Possible actions for Player p2:\n\n F Fire at a square\n M Move a ship to another square (" + p2.theBoard.movesRemaining() +" remaining)\n S Sonar scan ("+ p2.theBoard.sonarsRemaining() +" remaining)\n\nPlayer p2, what would you like to do?\n", bytes.toString());
    bytes.reset();

    char act2 = p2.readAction(p1);
    assertEquals(act2, 'M');

    char act3 = p2.readAction(p1);
    assertEquals(act3, 'S');
    

    assertThrows(EOFException.class, () -> p2.readAction(p1));

  }

  /** Test read Computer Human option
   */
  @Test
  public void test_readHuman() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = createTextPlayer(2,2,"C\ncom\nH\n", bytes);//player

    p1.chooseToBeHuman();
    assertFalse(p1.isHuman);
    p1.chooseToBeHuman();
    System.out.println(bytes.toString());//check error message for invalid input
    assertTrue(p1.isHuman);

    //check throws EOFException for null input
    assertThrows(EOFException.class, () -> p1.chooseToBeHuman());

  }

  
  /** Test the function playOneTurn (normal activities of fire and move)
   */
  @Test
  public void test_one_turn() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = attackingPhasePlayer(2,2,"p1",
                                         "M\nA0\nA1v\nM\nA1\nA1v\nM\nA1\nA1v\nm\nsiuuuu\nf\na0\nf\na0\n",
                                         bytes, new Coordinate("a0")); 
    TextPlayer p2 = attackingPhasePlayer(2,2,"p2","f\nA0\nf\nA1\nf\nb1\n\ns\na0\ns\na0\ns\na0\ns\nf\na0\n", bytes,
                                         new Coordinate("b1"));
    assertFalse(p1.lost());
    p1.playOneTurn(p2);//p1 moves ship
    p2.playOneTurn(p1);//p2 fires at old location
    assertFalse(p1.lost());
    p2.playOneTurn(p1);//p2 fires at new location
    p2.playOneTurn(p1);//p2 fires at new location
    assertTrue(p1.lost());
    bytes.reset();
    for(int i = 0; i<4; i++){
      p1.playOneTurn(p2);
      p2.playOneTurn(p1);
    }
    
    System.out.println(bytes.toString());
  }

  /** Test the computer play one turn result
   */
  @Test
  public void test_computer_one_turn() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = attackingPhasePlayer(5,5,"p1","C\nf\n", bytes, new Coordinate("a0")); 
    TextPlayer p2 = attackingPhasePlayer(5,5,"p2","C\nf\n", bytes, new Coordinate("b1"));

    p1.chooseToBeHuman();
    p2.chooseToBeHuman();
    for (int i = 0; i<20; i++){
      p1.playOneTurn(p2);
      p2.playOneTurn(p1);
      System.out.println(p1.view.displayMyOwnBoard());
      System.out.println(p2.view.displayMyOwnBoard());
      System.out.println(bytes.toString());
      bytes.reset();
    }
  }

  /** Test the move action exceptions
   */
  @Test
  public void test_Move() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = attackingPhasePlayer(2,2,"p1","A1\n", bytes,
                                         new Coordinate("a0")); 
    TextPlayer p2 = attackingPhasePlayer(2,2,"p2","A1\nA1h\na1\na0h\n", bytes,
                                         new Coordinate("a1"));
    //p1 moves ship
    assertThrows(IllegalArgumentException.class, () -> p1.actionMove());
    p2.actionMove();
    System.out.print(bytes.toString());
  }
  
  //-----------HELPER FUNCTIONS--------------
  /** Helper function for creating expected outputs of three added ships
   * @param expected: an array of Placement, returned by buildExpectedPlacements()
   * @param shipsToPlace: an array of ship names
   * @param shipCreationFns: a function to build ships
   * @returns a String for the expected output of BoardTextView after each add of a ship
   */
  private String buildExpectedOutputs(ArrayList<Placement> expected, ArrayList<String> shipsToPlace, HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns) {
    Board<Character> b = new BattleShipBoard<Character>(10,20, 'X');
    BoardTextView v = new BoardTextView(b);
    
    String expectedOutput = new String();
    for (int i = 0; i < expected.size(); i++){
      String ans = new String();
      Ship<Character> s  = shipCreationFns.get(shipsToPlace.get(i)).apply(expected.get(i));
      b.tryAddShip(s);
      ans += "Player A where do you want to place a " + shipsToPlace.get(i) + "?\n"
        + "Current ocean:\n" + v.displayMyOwnBoard();
      expectedOutput+=ans;
    }

    return expectedOutput;
  }
   
  /** Helper function for creating an array of expected placements of three added ships
   * @param a list of input string
   * @returns an array of Placements for the expected output of BoardTextView after each add of a ship
   */
  private ArrayList<Placement> buildExpectedPlacements(String[] lines) {
    ArrayList<Placement> expected = new ArrayList<Placement>();
    for (int i = 0; i < lines.length ;i++){
      Placement p = new Placement(lines[i]);//convert input strings into placements
      expected.add(p);
    }
    return expected;
  }

  /** helper function to create Text Player
   */
  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) throws IOException {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V2ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }

  /** helper function to create attacking phase player
   */

  private TextPlayer attackingPhasePlayer(int w, int h, String name, String inputData, OutputStream bytes, Coordinate... clist) throws IOException{
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V2ShipFactory();
    for (Coordinate c : clist){
      //build test ships
      board.tryAddShip(shipFactory.makeSubmarine(new Placement(c, 'v')));
    }
    
    return new TextPlayer(name, board, input, output, shipFactory);
  }

}
