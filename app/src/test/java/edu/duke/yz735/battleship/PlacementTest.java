package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {  
  /* this tests the constructor will assign correct values to the Placement
   */
  @Test
  public void test_valid_placement() {
    /* tests normal input */
    Coordinate c1 = new Coordinate(5,5);
    Placement p1 = new Placement(c1, 'H');
    Placement p2 = new Placement(c1, 'V');
    
    assertEquals(p1.getWhere(), c1);
    assertEquals(p1.getOrientation(), 'H');
    assertEquals(p2.getWhere(), c1);
    assertEquals(p2.getOrientation(), 'V');

    /* tests case insensitivity */
    Placement p3 = new Placement(c1, 'h');
    Placement p4 = new Placement(c1, 'v');
    
    assertEquals(p1.getOrientation(), p3.getOrientation());
    assertEquals(p2.getOrientation(), p4.getOrientation());

       /* tests normal input */
    Placement p5 = new Placement(c1, 'U');
    Placement p6 = new Placement(c1, 'R');
    
    assertEquals(p5.getOrientation(), 'U');
    assertEquals(p6.getOrientation(), 'R');

    /* tests case insensitivity */
    Placement p7 = new Placement(c1, 'u');
    Placement p8 = new Placement(c1, 'r');
    
    assertEquals(p7.getOrientation(), p5.getOrientation());
    assertEquals(p8.getOrientation(), p6.getOrientation());

    /* tests normal input */
    Placement p9 = new Placement(c1, 'D');
    Placement p10 = new Placement(c1, 'L');
    
    assertEquals(p9.getOrientation(), 'D');
    assertEquals(p10.getOrientation(), 'L');

    /* tests case insensitivity */
    Placement p11 = new Placement(c1, 'd');
    Placement p12 = new Placement(c1, 'l');
    
    assertEquals(p11.getOrientation(), p9.getOrientation());
    assertEquals(p12.getOrientation(), p10.getOrientation());
 
  }

  /* this tests the constructor will throw IllegalArgumentException
   * if input placement's orientaion is not valid
   * validity of Coordinate will be checked by Coordinate's constructor
   */
  @Test
  public void test_invalid_orientation() {
    Coordinate c1 = new Coordinate(5,5);
    assertThrows(IllegalArgumentException.class, () -> new Placement(c1,'a'));
  }

  /* this tests the constructor takes string input will assign correct values to the placement
   */
  @Test
  void test_string_constructor_valid_cases() {
    Placement p1 = new Placement("B3V");
    assertEquals('V', p1.getOrientation());
    assertEquals(1, p1.getWhere().getRow());
    assertEquals(3, p1.getWhere().getCol());
    Placement p2 = new Placement("D5v");
    assertEquals('V', p2.getOrientation());
    assertEquals(3, p2.getWhere().getRow());
    assertEquals(5, p2.getWhere().getCol());
    Placement p3 = new Placement("A9H");
    assertEquals('H', p3.getOrientation());
    assertEquals(0, p3.getWhere().getRow());
    assertEquals(9, p3.getWhere().getCol());
    Placement p4 = new Placement("Z0h");
    assertEquals('H', p4.getOrientation());
    assertEquals(25, p4.getWhere().getRow());
    assertEquals(0, p4.getWhere().getCol());
    Placement p5 = new Placement("B3U");
    assertEquals('U', p5.getOrientation());
    assertEquals(1, p5.getWhere().getRow());
    assertEquals(3, p5.getWhere().getCol());
    Placement p6 = new Placement("D5R");
    assertEquals('R', p6.getOrientation());
    assertEquals(3, p6.getWhere().getRow());
    assertEquals(5, p6.getWhere().getCol());
    Placement p7 = new Placement("A9D");
    assertEquals('D', p7.getOrientation());
    assertEquals(0, p7.getWhere().getRow());
    assertEquals(9, p7.getWhere().getCol());
    Placement p8 = new Placement("Z0L");
    assertEquals('L', p8.getOrientation());
    assertEquals(25, p8.getWhere().getRow());
    assertEquals(0, p8.getWhere().getCol());
  }

  /* this tests the constructor will throw IllegalArgumentException
   * if input string is in invalid format
   */
  @Test
  public void test_string_constructor_error_cases() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("00"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0w"));
  }
  
  /* this tests the equals() method is overrided successfully
   */
  @Test
  public void test_equals() {
    Coordinate c1 = new Coordinate(1, 2);
    Coordinate c2 = new Coordinate(1, 2);
    Placement p1 = new Placement(c1, 'h');
    Placement p2 = new Placement(c2, 'H');
    Coordinate c3 = new Coordinate(1, 3);
    Placement p3 = new Placement(c1, 'v');
    Placement p4 = new Placement(c3, 'h');
    
    assertEquals(p1, p1);   //equals should be reflexsive
    assertEquals(p1, p2);   //different objects bu same contents
    assertNotEquals(p1, p3);  //different contents
    assertNotEquals(p1, p4);
    assertNotEquals(p3, p4);
    assertNotEquals(p1, c1); //different types
  }

  /* this tests the hashCode() method is overrided successfully
   */
  @Test
  public void test_hashCode() {
    Coordinate c1 = new Coordinate(1, 2);
    Coordinate c2 = new Coordinate(1, 2);
    Placement p1 = new Placement(c1, 'h');
    Placement p2 = new Placement(c2, 'H');
    Coordinate c3 = new Coordinate(0, 3);
    Placement p3 = new Placement(c1, 'v');
    Placement p4 = new Placement(c3, 'h');
    assertEquals(p1.hashCode(), p2.hashCode());
    assertNotEquals(p1.hashCode(), p3.hashCode());
    assertNotEquals(p1.hashCode(), p4.hashCode());
  }
}
