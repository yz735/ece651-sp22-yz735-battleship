package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_getPivot() {
    /** Tests the method getPivot
     */

    //Define dimensions of the ship
    Coordinate upperLeft = new Coordinate(1,2);
    pivotBuilder(upperLeft, 2, 3, "UDRL", new Coordinate(1, 2), new Coordinate(3, 3), new Coordinate(1, 4), new Coordinate(2, 2));
    pivotBuilder(upperLeft, 1, 3, "HV", new Coordinate(1, 2), new Coordinate(1, 2));
  }

  private void pivotBuilder(Coordinate upperLeft, int width, int height, String os, Coordinate... expected) {
    /** Helper function to test different orientation and corresponding pivot
     */
    int index = 0;
    for (Coordinate c: expected){
      Placement p = new Placement(upperLeft, os.charAt(index));
      Coordinate pivot = RectangleShip.getPivot(p, width, height);
      assertEquals(pivot, c);
      index++;
    }
  }

  @Test
  public void test_makeCoords() {
    /** Tests the method makeCoords
     */

    //define shape of the ship
    /** ss
        s
        ss Up
    */
    Coordinate upperLeft = new Coordinate(1,2);
    int width = 2;
    int height = 3;
    //define missing pieces of the ship
    HashSet<Integer> toRemove = new HashSet<Integer>();
    toRemove.add(3);

    //test Up
    Placement pU = new Placement(upperLeft, 'U');
    Coordinate pivotU = RectangleShip.getPivot(pU, width, height);
    ArrayList<Coordinate> set = RectangleShip.makeCoords(pU, width, height, toRemove);
    
    int index = 0;
    int setIndex = 0;
    for (int i = 0; i < height; i++){
      for (int j = 0; j < width; j++){
        Coordinate c = new Coordinate(pivotU.getRow() + i, pivotU.getCol() + j);
        if (!toRemove.contains(index)){
          assertEquals(c, set.get(setIndex));
          setIndex++;
        }
        else{
          assertFalse(set.contains(c));
        }
        index++;
      }
    }
  }

  @Test
  public void test_get_coords(){
    /** Tests the function getCoordinates
     */
    //define shape of the ship
    /** ss
        s
        ss Up
    */
    Coordinate upperLeft = new Coordinate(1,2);
    int width = 2;
    int height = 3;
    //define missing pieces of the ship
    HashSet<Integer> toRemove = new HashSet<Integer>();
    toRemove.add(3);

    //test Up
    Placement pU = new Placement(upperLeft, 'R');
    Ship<Character> s = new RectangleShip("doubleHead",pU, width, height, 'D', '*', toRemove); 
    for (Coordinate c : s.getCoordinates()){
      String str_c = new String();
      str_c+="("+c.getRow()+","+c.getCol()+")";
      System.out.println(str_c);
    }
  }

  
  @Test
  public void test_occupy_coords() {
    /** Tests the function occupyCoordinates
     */

    //define shape of the ship
    /** ss
        s
        ss Up
    */
    Coordinate upperLeft = new Coordinate(1,2);
    int width = 2;
    int height = 3;
    //define missing pieces of the ship
    HashSet<Integer> toRemove = new HashSet<Integer>();
    toRemove.add(4);

    //test Up
    Placement pU = new Placement(upperLeft, 'U');
 
    //construct ship (with missing pieces)
    RectangleShip<Character> rs = new RectangleShip<Character>("boat", pU, width, height, 's', '*', toRemove);

    //get the Coordinate pieces of the ship
    ArrayList<Coordinate> keySet = (ArrayList<Coordinate>)rs.getCoordinates();
    ArrayList<Coordinate> set = RectangleShip.makeCoords(pU, width, height, toRemove);
    int index = 0;
    for (Coordinate c: keySet){
      assertEquals(c, set.get(index));
      index++;
    }
    
    Coordinate notToAdd = new Coordinate(upperLeft.getRow() + height, upperLeft.getCol() + width);
    assertFalse(rs.occupiesCoordinates(notToAdd));
  }

  @Test
  public void test_hit(){
    Coordinate upperLeft = new Coordinate(1,2);
    int width = 1;
    int height = 3;
    Coordinate hit1 = new Coordinate(2,2);
    RectangleShip<Character> rs = new RectangleShip<Character>("boat", upperLeft, width, height, 's', '*');
    rs.recordHitAt(hit1);
    assertTrue(rs.wasHitAt(hit1));
    assertEquals(rs.getDisplayInfoAt(hit1,true), '*');
    assertEquals(rs.getDisplayInfoAt(hit1,false), 's');
    assertFalse(rs.wasHitAt(upperLeft));
    assertEquals(rs.getDisplayInfoAt(upperLeft,true), 's');
    assertEquals(rs.getDisplayInfoAt(upperLeft,false), null);
  }

  @Test
  public void test_not_hit(){
    Coordinate upperLeft = new Coordinate(1,2);
    int width = 1;
    int height = 3;
    Coordinate hit1 = new Coordinate(1,3);
    RectangleShip<Character> rs = new RectangleShip<Character>("boat", upperLeft, width, height, 's', '*');
    assertThrows(IllegalArgumentException.class, () -> rs.recordHitAt(hit1));
    assertThrows(IllegalArgumentException.class, () -> rs.wasHitAt(hit1));
    assertThrows(IllegalArgumentException.class, () -> rs.getDisplayInfoAt(hit1,true));
  }

  @Test
  public void test_sunk(){
    Coordinate upperLeft = new Coordinate(1,2);
    RectangleShip<Character> rs = new RectangleShip<Character>(upperLeft, 's', '*');
    rs.recordHitAt(upperLeft);
    assertTrue(rs.isSunk());
  }

  @Test
  public void test_not_sunk(){
    Coordinate upperLeft = new Coordinate(1,2);
    int width = 1;
    int height = 3;
    Coordinate hit1 = new Coordinate(2,2);
    RectangleShip<Character> rs = new RectangleShip<Character>("boat", upperLeft, width, height, 's', '*');
    rs.recordHitAt(hit1);
    assertFalse(rs.isSunk());
  }
  
}

