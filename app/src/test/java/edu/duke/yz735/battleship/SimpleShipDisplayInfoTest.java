package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_getInfo() {
    SimpleShipDisplayInfo<Character> ssdi = new SimpleShipDisplayInfo<Character>('a', 'b');
    Coordinate where = new Coordinate(1,2);
    assertEquals(ssdi.getInfo(where, false), 'a');
    assertEquals(ssdi.getInfo(where, true), 'b');
    System.out.println("Welcome");
  }

}
