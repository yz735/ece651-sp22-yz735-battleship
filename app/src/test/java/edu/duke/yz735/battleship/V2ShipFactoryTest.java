package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {
  private void checkShip(Ship<Character> testShip, String expectedName,
                          char expectedLetter, Coordinate... expectedLocs){
     assertEquals(testShip.getName(), expectedName);
     for (Coordinate c : expectedLocs){
       assertEquals(testShip.getDisplayInfoAt(c,true), expectedLetter);
     }
   }
  
  @Test
  public void test_ShipFactory() {
    /** Test the methods in V1ShipFactory
     */
    V1ShipFactory f = new V2ShipFactory();

    /*test V1 ships*/
    Placement w1 = new Placement("A0V");

    Ship<Character> sub = f.makeSubmarine(w1);
    checkShip(sub, "Submarine", 's', new Coordinate("A0"), new Coordinate("B0"));
    assertFalse(sub.occupiesCoordinates(new Coordinate("C0")));

    Ship<Character> dst = f.makeDestroyer(w1);
    checkShip(dst, "Destroyer", 'd', new Coordinate("A0"), new Coordinate("B0"), new Coordinate("C0"));
    assertFalse(dst.occupiesCoordinates(new Coordinate("C3")));

    /*test V2 ships*/
    //test Up(U)
    Placement w2U = new Placement("B3U");

    Ship<Character> carU = f.makeCarrier(w2U);
    checkShip(carU, "Carrier", 'c', new Coordinate("B3"), new Coordinate("C3"), new Coordinate("D3"), new Coordinate("D4"), new Coordinate("E3"), new Coordinate("E4"),new Coordinate("F4"));
    assertFalse(carU.occupiesCoordinates(new Coordinate("G0")));
    assertFalse(carU.occupiesCoordinates(new Coordinate("B4")));
    assertFalse(carU.occupiesCoordinates(new Coordinate("C4")));
    assertFalse(carU.occupiesCoordinates(new Coordinate("F5")));
     
    Ship<Character> batU = f.makeBattleship(w2U);
    checkShip(batU, "Battleship", 'b', new Coordinate("B4"), new Coordinate("C3"), new Coordinate("C4"), new Coordinate("C5"));
    assertFalse(batU.occupiesCoordinates(new Coordinate("B3")));
    assertFalse(batU.occupiesCoordinates(new Coordinate("B5")));
    assertFalse(batU.occupiesCoordinates(new Coordinate("G0")));

    //test Right(R)
    Placement w2R = new Placement("B3R");

    Ship<Character> carR = f.makeCarrier(w2R);
    checkShip(carR, "Carrier", 'c', new Coordinate("B4"), new Coordinate("B5"), new Coordinate("B6"), new Coordinate("B7"), new Coordinate("C3"), new Coordinate("C4"),new Coordinate("C5"));
    assertFalse(carR.occupiesCoordinates(new Coordinate("G0")));
    assertFalse(carR.occupiesCoordinates(new Coordinate("B3")));
    assertFalse(carR.occupiesCoordinates(new Coordinate("C6")));
    assertFalse(carR.occupiesCoordinates(new Coordinate("C7")));
     
    Ship<Character> batR = f.makeBattleship(w2R);
    checkShip(batR, "Battleship", 'b', new Coordinate("B3"), new Coordinate("C3"), new Coordinate("D3"), new Coordinate("C4"));
    assertFalse(batR.occupiesCoordinates(new Coordinate("B4")));
    assertFalse(batR.occupiesCoordinates(new Coordinate("D4")));
    assertFalse(batR.occupiesCoordinates(new Coordinate("G0")));

    //test Down(D)
    Placement w2D = new Placement("B3D");

    Ship<Character> carD = f.makeCarrier(w2D);
    checkShip(carD, "Carrier", 'c', new Coordinate("B3"), new Coordinate("C3"), new Coordinate("D3"), new Coordinate("C4"), new Coordinate("D4"), new Coordinate("E4"),new Coordinate("F4"));
    assertFalse(carD.occupiesCoordinates(new Coordinate("G0")));
    assertFalse(carD.occupiesCoordinates(new Coordinate("B4")));
    assertFalse(carD.occupiesCoordinates(new Coordinate("E3")));
    assertFalse(carD.occupiesCoordinates(new Coordinate("F3")));
     
    Ship<Character> batD = f.makeBattleship(w2D);
    checkShip(batD, "Battleship", 'b', new Coordinate("B3"), new Coordinate("B4"), new Coordinate("C4"), new Coordinate("B5"));
    assertFalse(batD.occupiesCoordinates(new Coordinate("C3")));
    assertFalse(batD.occupiesCoordinates(new Coordinate("C5")));
    assertFalse(batD.occupiesCoordinates(new Coordinate("G0")));

    //test Left(L)
    Placement w2L = new Placement("B3L");
    
    Ship<Character> carL = f.makeCarrier(w2L);
    checkShip(carL, "Carrier", 'c', new Coordinate("B5"), new Coordinate("B6"), new Coordinate("B7"), new Coordinate("C3"), new Coordinate("C4"), new Coordinate("C5"),new Coordinate("C6"));
    assertFalse(carL.occupiesCoordinates(new Coordinate("G0")));
    assertFalse(carL.occupiesCoordinates(new Coordinate("B3")));
    assertFalse(carL.occupiesCoordinates(new Coordinate("B4")));
    assertFalse(carL.occupiesCoordinates(new Coordinate("C7")));
     
    Ship<Character> batL = f.makeBattleship(w2L) ;
    checkShip(batL, "Battleship", 'b', new Coordinate("B4"), new Coordinate("C3"), new Coordinate("C4"), new Coordinate("D4"));
    assertFalse(batL.occupiesCoordinates(new Coordinate("B3")));
    assertFalse(batL.occupiesCoordinates(new Coordinate("D3")));
    assertFalse(batL.occupiesCoordinates(new Coordinate("G0")));

    /*test invalid V2 ships*/
    //test Horizontal(H) and Vertical(V)
    Placement wH = new Placement("A0H");
    Placement wV = new Placement("A0V");

    assertThrows(IllegalArgumentException.class, () -> f.makeBattleship(wH));
    assertThrows(IllegalArgumentException.class, () -> f.makeCarrier(wV));
  }

}
