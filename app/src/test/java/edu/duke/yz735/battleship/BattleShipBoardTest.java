package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  /* this tests the constructor of BattleShipBoard assigns correct width and height */
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  /** this tests the constructor of BattleShipBoard<Character> will 
   * @throws IllegalArgumentException if input dimensions 
   * are not positive
   */
  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  /** this tests the whatIsAt 
   * @returns null for all coordinates in an empty Board 
   */
  @Test
  public void test_whatIsAt_empty() {
    Board<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    checkWhatIsAtBoard(b1, null);
  }

  @Test
  public void test_whatIsAt_addShip() {
    Board<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    Coordinate cToAdd = new Coordinate(3, 3);
    RectangleShip<Character> shipToAdd = new RectangleShip<Character>(cToAdd, 's', '*');
    RectangleShip<Character> shipNotToAdd = new RectangleShip<Character>(new Coordinate(5, 5), 's', '*');
    
    assertEquals(b1.tryAddShip(shipToAdd),null);
    assertNotEquals(b1.tryAddShip(shipToAdd),null);//not again!
    assertNotEquals(b1.tryAddShip(shipNotToAdd),null);//not on the board!
    
    Character[][] expected = expectedAddShip(b1, cToAdd);
    checkWhatIsAtBoard(b1, expected);
  }

  @Test
  public void test_fireAt(){
    Board<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    V1ShipFactory f1 = new V2ShipFactory();
    Ship<Character> s1 = f1.makeSubmarine(new Placement("B0H"));
    b1.tryAddShip(s1);

    Coordinate c1 = new Coordinate("B0");//first hit
    Coordinate c2 = new Coordinate("A1");//miss
    Coordinate c3 = new Coordinate("B1");//second hit
    
    assertSame(s1, b1.fireAt(c1));//assert our submarine #1 gets hit
    assertEquals(s1.getDisplayInfoAt(c1,true), '*');//assert the place get hit is correct
    assertEquals(b1.whatIsAtForSelf(c1), '*');
    assertEquals(b1.whatIsAtForEnemy(c1), 's');
    assertEquals(s1.getDisplayInfoAt(c3,true), 's');
    assertFalse(s1.isSunk());//assert our submarine #1 is not sunk yet
    assertSame(null, b1.fireAt(c2));//assert miss
    assertEquals(b1.whatIsAtForSelf(c2), null);
    assertEquals(b1.whatIsAtForEnemy(c2), 'X');
    assertFalse(s1.isSunk());//assert our submarine #1 is not sunk yet
    assertSame(s1, b1.fireAt(c3));
    assertEquals(b1.whatIsAtForSelf(c3), '*');
    assertEquals(b1.whatIsAtForEnemy(c3), 's');
    assertTrue(s1.isSunk());//assert our submarine #1 is sunk!
  }

  @Test
  public void test_tryMoveTo(){
    Board<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    assertEquals(b1.movesRemaining(), 3);//total number of moves starts with 3
    
    V1ShipFactory f1 = new V2ShipFactory();
    Ship<Character> s1 = f1.makeBattleship(new Placement("B0R"));
    b1.tryAddShip(s1);
    
    Coordinate c1 = new Coordinate("B0");//first hit
    Coordinate c2 = new Coordinate("C1");//second hit

    b1.fireAt(c1);
    b1.fireAt(c2);
    
    Ship<Character> s2 = f1.makeBattleship(new Placement("C2U"));
    assertEquals(b1.whatIsAtForSelf(c1), '*');
    assertEquals(b1.whatIsAtForSelf(new Coordinate("C0")), 'b');
    assertEquals(b1.whatIsAtForSelf(c2), '*');
    
    b1.tryMoveTo(s1, s2);
    assertEquals(b1.whatIsAtForSelf(new Coordinate("B0")), null);
    assertEquals(b1.whatIsAtForSelf(new Coordinate("C0")), null);
    assertEquals(b1.whatIsAtForSelf(new Coordinate("d3")), 'b');

    assertTrue(s2.wasHitAt(new Coordinate("c3")));
    assertTrue(s2.wasHitAt(new Coordinate("d2")));

    assertEquals(b1.movesRemaining(), 2);//used one move
    
    //try illegal movement from one type of ship to another type of ship
    Ship<Character> s3 = f1.makeDestroyer(new Placement("C2H"));
    assertThrows(IllegalArgumentException.class, () -> b1.tryMoveTo(s1, s3));
    
    //try illegal movement to a invalid placement
    Ship<Character> s4 = f1.makeBattleship(new Placement("C5R"));
    assertNotEquals(b1.tryMoveTo(s1, s4),null);
    assertEquals(b1.whatIsAtForSelf(new Coordinate("C0")), 'b');
    
  }

  @Test
  public void test_getScanRegion(){
    /** printout coordinates to see if the scanregion returns the right set of coordinates
     */
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    HashSet<Coordinate> scanRegion = b1.getScanRegion(new Coordinate("D4"));
    for (Coordinate c: scanRegion){
      System.out.println("("+c.getRow()+","+c.getCol()+")");
    }
  }

  @Test
  public void test_sonarScan(){
    /**
     * test sonar scan return the correct results
     */
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory f1 = new V2ShipFactory();
    Ship<Character> s1 = f1.makeBattleship(new Placement("B2R"));
    Ship<Character> s2 = f1.makeBattleship(new Placement("G6U"));
    Ship<Character> s3 = f1.makeCarrier(new Placement("D3L"));
    Ship<Character> s4 = f1.makeSubmarine(new Placement("A5V"));
    Ship<Character> s5 = f1.makeDestroyer(new Placement("F1H"));
    
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);
    b1.tryAddShip(s3);
    b1.tryAddShip(s4);
    b1.tryAddShip(s5);

    BoardTextView view = new BoardTextView(b1);
    System.out.println(view.displayMyOwnBoard());
    
    StringBuilder sb = new StringBuilder();
    sb.append("Submarines occupy 1 squares\n");
    sb.append("Destroyers occupy 1 squares\n");
    sb.append("Battleships occupy 3 squares\n");
    sb.append("Carriers occupy 7 squares\n");

    assertEquals(b1.sonarsRemaining(), 3);
    assertEquals(b1.sonarScan(new Coordinate("D4")), sb.toString());
    assertEquals(b1.sonarsRemaining(), 2);
  }
  
  @Test
  public void test_selectShip(){
    /**
     * test if selectShip selects the ship we want
     */

    //buildup board
    Board<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    
    V1ShipFactory f1 = new V2ShipFactory();
    Ship<Character> s1 = f1.makeBattleship(new Placement("B0R"));
    b1.tryAddShip(s1);

    assertSame(b1.selectShip(new Coordinate("C1")), s1);
    assertNull(b1.selectShip(new Coordinate("B1")));
  }
  
  @Test
  public void test_AllSunk(){
    Board<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    Coordinate c1 = new Coordinate("a0");
    Coordinate c2 = new Coordinate("c3");
    RectangleShip<Character> ts1 = new RectangleShip<Character>(c1, 's', '*');
    b1.tryAddShip(ts1);
    RectangleShip<Character> ts2 = new RectangleShip<Character>(c2, 's', '*');
    b1.tryAddShip(ts2);
    
    b1.fireAt(c1);
    assertFalse(b1.checkAllSunk());

    b1.fireAt(c2);
    assertTrue(b1.checkAllSunk());
  }

  /** Helper function: Build a matrix of the expected return value of tryAddShip at each location
   */
  private <T> Character[][] expectedAddShip(Board<T> b, Coordinate c){
    Character[][] expected = new Character[b.getHeight()][b.getWidth()];
    for (int i = 0; i < b.getHeight(); i++){
      for (int j = 0; j < b.getWidth(); j++){
        if (i == c.getRow() && j == c.getCol()){
          expected[i][j] = 's';
        }
        else{
          expected[i][j] = null;
        }
      }
    }
    return expected;
  }

  /** Helper function: check if the board.whatIsAt returns same value of at each location with expected
   */
  private <T> void checkWhatIsAtBoard(Board<T> b, T[][] expected){
    for (int i = 0; i < b.getHeight(); i++){
      for (int j = 0; j < b.getWidth(); j++){
        Coordinate c = new Coordinate(i,j);
        if (expected == null){
          assertEquals(b.whatIsAtForSelf(c), null);
          assertEquals(b.whatIsAtForEnemy(c),null);
        }
        else {
          assertEquals(b.whatIsAtForSelf(c), expected[i][j]);
          assertEquals(b.whatIsAtForEnemy(c), null);
        }
      } 
    }
  }

}

