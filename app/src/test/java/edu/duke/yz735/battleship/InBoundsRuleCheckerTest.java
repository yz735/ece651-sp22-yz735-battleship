package edu.duke.yz735.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_Bound_Checker() {
    PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
    Board<Character> b1 = new BattleShipBoard<Character>(5, 10, 'X');
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate (-1, 0), 's', '*');//goes over top
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate (0, -1), 's', '*');//goes over left
    Ship<Character> s3 = new RectangleShip<Character>(new Coordinate (10, 0), 's', '*');//goes over bottom
    Ship<Character> s4 = new RectangleShip<Character>(new Coordinate (0, 5), 's', '*');//goes over right
    Ship<Character> s5 = new RectangleShip<Character>(new Coordinate (0, 0), 's', '*');//valid placement
    
    assertEquals(checker.checkPlacement(s1,b1),"That placement is invalid: the ship goes off the top of the board.");
    assertEquals(checker.checkPlacement(s2,b1),"That placement is invalid: the ship goes off the left of the board.");
    assertEquals(checker.checkPlacement(s3,b1),"That placement is invalid: the ship goes off the bottom of the board.");
    assertEquals(checker.checkPlacement(s4,b1),"That placement is invalid: the ship goes off the right of the board.");
    
    assertEquals(checker.checkPlacement(s5,b1),null);
  }
}
