package edu.duke.yz735.battleship;

public abstract class PlacementRuleChecker<T> {
  /** Chain of rules
   */
  private final PlacementRuleChecker<T> next;
  //more stuff

  public PlacementRuleChecker(PlacementRuleChecker<T> next) {
    this.next = next;
  }
  
  /**
   * Subclasses will override this method to specify how they check their own rule.
   */
  protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

  /**
   * checks placement rules: on the board
   */
  public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
    //if we fail our own rule: stop the placement is not legal
    String this_message = checkMyRule(theShip, theBoard);
    if (this_message!=null) {
      return this_message;
    }
    //other wise, ask the rest of the chain.
    if (next != null) {
      return next.checkPlacement(theShip, theBoard); 
    }
    //if there are no more rules, then the placement is legal
    return null;
  }
}
