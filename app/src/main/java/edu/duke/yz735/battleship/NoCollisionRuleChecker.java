package edu.duke.yz735.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    /** check the placement of theShip onto theBoard doesnot collide with other ships on theBoard
     * @param theShip: the ship we place
     * @param theBoard: the board we play on
     * @returns true if satisfied my rules; false if not
     */
    for (Coordinate c : theShip.getCoordinates()){
      if (theBoard.whatIsAtForSelf(c) != null) {
        return "That placement is invalid: the ship overlaps another ship.";
      }
    }    
    return null;
  }

  public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
  
  
}
