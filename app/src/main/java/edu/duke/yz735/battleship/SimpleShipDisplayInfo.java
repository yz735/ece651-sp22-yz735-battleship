package edu.duke.yz735.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
  private final T myData;
  private final T onHit;

  public SimpleShipDisplayInfo (T data, T hit){
    this.myData = data;
    this.onHit = hit;
  }
  
  @Override
  public T getInfo(Coordinate where, boolean hit) {
    // TODO Auto-generated method stub
    if (hit){
      return onHit;
    }
    return myData;
  }
  
}
