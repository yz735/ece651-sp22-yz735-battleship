package edu.duke.yz735.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class TextPlayer {
  final Board<Character> theBoard;
  final BoardTextView view;
  final BufferedReader inputReader;
  final PrintStream out;
  final AbstractShipFactory<Character> shipFactory;
  final String name;
  final ArrayList<String> shipsToPlace;
  final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
  boolean isHuman;

  //CONSTRUCTORS
  public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> factory) throws IOException{
    /** This is the constructor
     */
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.inputReader = inputSource;
    this.out = out;
    this.shipFactory = factory;
    this.name = name;
    this.isHuman = true;
    this.shipsToPlace = new ArrayList<String>();
    this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
    setupShipCreationMap();
    setupShipCreationList();
  }

  //choose to be a human!
  public void chooseToBeHuman() throws IOException{
    out.println("Player "+name+" is a:\n H human\n C computer\nPlease type H or C to chooose");
    String s = "I am a cyborg.";
    while(true){
      s = inputReader.readLine();
      if (s==null){
        throw new EOFException("EOF error");
      }
      char choice = s.toUpperCase().charAt(0);
      if (choice == 'H' && s.length()==1){
        isHuman = true;
        break;
      }
      if (choice == 'C' && s.length()==1){
        isHuman = false;
        break;
      }
      else{
        out.println("Invalid input, please try again");
      }
    }
  }
  
  //PLACEMENT PHASE
  public Placement readPlacement(String prompt) throws IOException {
    /** This method reads a Placement 
     * @param prompt: a string of prompt sentence
     * @returns a new Placement constructed from a line in inputReader
     */
    out.println(prompt);
    String s = inputReader.readLine();
    if (s==null){
      throw new EOFException("EOF error");
    }
    return new Placement(s);
  }

  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    /** This method performs one Placement of a ship on the board
     * @param shipName: name of the ship. i.e. Submarine
     * @param createFn: the function to be called to create the ship
     */
    boolean retry = true;
    Placement p;
    //continue to retry the input if tryAddShip failed (return value not equal to null)
    while(retry){
      //this try/catch handles the invalid placements
      try{
        p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
        //thus throws IllegalArgumentException from Coordinate Constructors and
        //goes over the board
        Ship<Character> s = createFn.apply(p);
        String tryAddMsg = theBoard.tryAddShip(s);
        //this if/else handles if part of the ship goes over the board (but all coordinates valid)
        if (tryAddMsg==null){
          out.println("Current ocean:");
          out.print(view.displayMyOwnBoard());
          retry = false;//end the loop
        }
        else{
          out.println(tryAddMsg);//print out what's wrong
        }
      }
      catch(IllegalArgumentException iae){
        out.println("That placement is invalid: it does not have the correct format.");
      }
    }  
  }
  
  public void doPlacementPhase() throws IOException{
    if (isHuman){
      humanDoPlacementPhase();
    }
    else{
      computerDoPlacementPhase();
    }
  }

  //do all placements as human
  public void humanDoPlacementPhase() throws IOException{
    /** print the Board before placement
     * print the prompt
     * do the placement for each ship
     */
    out.print(view.displayMyOwnBoard());
    out.print("Player " +name + ": you are going to place the following ships (which are all\nrectangular). For each ship, type the coordinate of the upper left\nside of the ship, followed by the orientation of the ship you want.\nSubmarines and Destroyers have 2 orientations. These two orientations\nare horizontal (H) and vertical (V). Battleships and Carriers have\n4 orientations. These four orientations are now up (U), right (R), \ndown (D), and left (L).For example M4H would place a ship horizontally starting\nat M4 and going to the right.  You have\n\n2 \"Submarines\" ships that are 1x2 \n3 \"Destroyers\" that are 1x3\n3 \"Battleships\" that are 1x4\n2 \"Carriers\" that are 1x6\n");
    for (String shipName : shipsToPlace){      
      doOnePlacement(shipName, shipCreationFns.get(shipName));
    }
  }

  //do all placements as computer
  public void computerDoPlacementPhase() {
    for (String shipName : shipsToPlace){
      randomPlacement(shipName, shipCreationFns.get(shipName));
    }
  }

  //do random placements as computer
  public void randomPlacement (String shipName, Function<Placement, Ship<Character>> createFn){
    Placement p = new Placement(randomCoordinate(),
                                randomChar('h','v','u','d','r','l'));
    while(true){
      try{
        Ship<Character> s = createFn.apply(p);
        String tryAddMsg = theBoard.tryAddShip(s);
        if (tryAddMsg!=null){
          throw new IllegalArgumentException();
        }
        break;
      }
      catch(IllegalArgumentException iae){
        p = new Placement(randomCoordinate(), randomChar('h','v','u','d','r','l'));
      }
    }
  }
  
  //ATTACKING PHASE
  public char readAction(TextPlayer enemy) throws IOException{
    /** This method reads an action from:
     * F Fire at a square
     * M Move a ship to another square (2 remaining)
     * S Sonar scan (1 remaining)
     * if an action has ran out of 3 uses, will not show as an option
     * @return the option in char: (F, M, S)
     */
    out.println("Possible actions for Player " + name +":\n");
    out.println(" F Fire at a square");
    int nMoves = theBoard.movesRemaining();
    if (nMoves > 0){
      out.println(" M Move a ship to another square (" + nMoves +" remaining)");
    }
    int nSonar = enemy.theBoard.sonarsRemaining();
    if (nSonar > 0){
      out.println(" S Sonar scan ("+ nSonar +" remaining)");
    }
    out.println("\nPlayer "+ name +", what would you like to do?");
    
    boolean retry = true;
    char action = 'F';
    while(retry){
      String s = inputReader.readLine();
      if (s==null){
        throw new EOFException("EOF error");
      }
      if (s.length()==1){
        action = s.toUpperCase().charAt(0);
      }
      if(action!='F' && action!='M' && action!='S' || s.length()!=1){
        out.println("Input action invalid: must be F, M, or S!");
        continue;
      }
      if(action=='M' && nMoves==0){
        out.println("You have ran out of ship movements!");
        continue;
      }
      if(action=='S' && nSonar==0){
        out.println("You have ran out of Sonar!");
        continue;
      }
      retry = false;
    }
    
    return action;
  }
  
  public Coordinate readCoordinate(String prompt) throws IOException {
    /** This method reads a Coordinate 
     * @param prompt: a string of prompt sentence
     * @returns a new Coordinate constructed from a line in inputReader
     * @throws IOException if input coordinate invalid
     */
    out.println(prompt);
    String s = inputReader.readLine();
    if (s==null){
      throw new EOFException("EOF error");
    }
    Coordinate c = new Coordinate(s);
    if(c.getRow()<0 ||
       c.getRow() >= theBoard.getHeight() ||
       c.getCol()<0 ||
       c.getCol()>=theBoard.getWidth()){
      throw new IllegalArgumentException("This coordinate is not on the board!");
    }
    return c;
  }

  public void playOneTurn(TextPlayer enemy) throws IOException{
    if (isHuman){
      humanPlayOneTurn(enemy);
    }
    else{
      computerPlayOneTurn(enemy);
    }
  }

  //COMPUTER ATTACKING PHASE
  public void computerPlayOneTurn(TextPlayer enemy){
    /**
     * This method operates one turn of attack for this player as a computer
     * @param enemy: enemy player
     */
    char action = randomAction(enemy);
    if (action=='F'){
      randomFire(enemy);
    }
    if (action=='M'){
      randomMove();
    }
    if (action=='S'){
      randomSonar(enemy);
    }
  }

  private void randomFire(TextPlayer enemy) {
    //randomly fire at a location
    Coordinate where = randomCoordinate();
    while (enemy.theBoard.firedBefore(where)){
      where = randomCoordinate();
    }
    Ship<Character> s = enemy.theBoard.fireAt(where);
    if (s==null){
      out.println("Player "+name+" missed!");
    }
    else{
      out.println("Player "+name+" hit your " + s.getName() + " at " + where.toString() + "!");
    }
  }

  private void randomMove(){
    out.println("Player "+name+" used a special action!");
    Ship<Character> oldShip = null;
    while(oldShip==null){
       oldShip= theBoard.selectShip(randomCoordinate());
    }
    
    Placement p = new Placement(randomCoordinate(),
                                randomChar('h','v','u','d','r','l'));
    while(true){
      try{
        Ship<Character>newShip = shipCreationFns.get(oldShip.getName()).apply(p);
        String tryMoveMsg = theBoard.tryMoveTo(oldShip, newShip);
        if (tryMoveMsg!=null){
          throw new IllegalArgumentException();
        }
        break;
      }
      catch(IllegalArgumentException iae){
        p = new Placement(randomCoordinate(), randomChar('h','v','u','d','r','l'));
      }
    }
    
  }

  private void randomSonar(TextPlayer enemy) {
    //use sonar at a random location
    out.println("Player "+name+" used a special action!");
    enemy.theBoard.sonarScan(randomCoordinate());
  }

  
  //HUMAN ATTACKING PHASE
  public void humanPlayOneTurn(TextPlayer enemy) throws IOException{
    /**
     * This method operates one turn of attack for this player as a human
     * @param enemy: enemy player
     */
    out.println("Player " + name + "\'s turn");
    out.print(view.displayMyBoardWithEnemyNextToIt(enemy.view, "Your Ocean", "Player "+ enemy.name + "\'s Ocean"));

    char action = 'F';
    if (theBoard.movesRemaining()>0 || enemy.theBoard.sonarsRemaining()>0){ 
      action = readAction(enemy);
    }
    boolean retry = true;
    while(retry){
      //This try catch displays any illegal input error messages for actions
      try{ 
        if (action=='F'){
          actionFire(enemy);
        }
        if (action=='M'){
          actionMove();
        }
        if (action=='S'){
          actionSonar(enemy);
        }
        retry = false;
      }
      catch(IllegalArgumentException iae){
        int offset = iae.toString().indexOf(":")+2;
        //Display with only message and without exception 
        out.println(iae.toString().substring(offset));
      }
    }
  }

  //ACTIONS F M S
  public void actionFire(TextPlayer enemy) throws IOException {
    /** This method operate one Fire action towards the enemy board
     */
    Coordinate c = readCoordinate("Player " + name + " where do you want to fire at?");
    Ship<Character> s = enemy.theBoard.fireAt(c);
    if (s==null){
      out.println("You missed!");
    }
    else{
      out.println("You hit a " + s.getName() + "!");
    }
  }
  
  public void actionMove() throws IOException{
    /** This method operate one move action on this player's board
     */
    Coordinate c = readCoordinate("Please enter the Coordinate occupied by the ship you want to move:");
    Ship<Character> oldShip = theBoard.selectShip(c);
    if (oldShip == null){
      throw new IllegalArgumentException("This coordinate is not occupied by any ship!");
    }    
    
    Placement p;
    boolean retry = true;
    while(retry){
      try{
        p = readPlacement("Please enter the new Placement you want to move to:");
        Ship<Character> newShip = shipCreationFns.get(oldShip.getName()).apply(p);
        String tryMoveMsg = theBoard.tryMoveTo(oldShip, newShip);
        if (tryMoveMsg != null){
          out.println(tryMoveMsg);
        }
        else{
          retry = false;
        }
      }
      catch (IllegalArgumentException iae){
        int offset = iae.toString().indexOf(":")+2;
        //Display with only message and without exception 
        out.println(iae.toString().substring(offset));
      }
    }
  }

  public void actionSonar(TextPlayer enemy) throws IOException {
    /** This method operate one sonar action on the enemy's board
     */
    Coordinate c = readCoordinate("Player " + name + " where do you want to scan?");
    String s = enemy.theBoard.sonarScan(c);
    out.print(s);
  }


  //WIN/LOST STATUS
  public boolean lost(){
    /** check if this player has lost (all ship sunk)x
     */
    if (theBoard.checkAllSunk()){
      return true;
    }
    return false;
  }

  //HELPER FUNCTIONS
  protected void setupShipCreationMap(){
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
  }

  protected void setupShipCreationList(){
    shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
    shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
  }

  private char randomChar(char... chars) {
    //returns a random Character from the list of Characters
    Random rand = new Random();
    return chars[rand.nextInt(chars.length)];
  }

  private Coordinate randomCoordinate(){
    //returns a random coordinate on the board
    Random rand = new Random();
    return new Coordinate(rand.nextInt(theBoard.getHeight()),
                   rand.nextInt(theBoard.getWidth()));
  }

  private char randomAction(TextPlayer enemy) {
    /**
     * @return a random action for computer
     */
    char[] actions = {'F','F','F'};
    if (theBoard.movesRemaining()>0){
      actions[1] = 'M';
    }
    if (enemy.theBoard.sonarsRemaining()>0){
      actions[2] = 'S';
    }
    return randomChar(actions);
  }
}
