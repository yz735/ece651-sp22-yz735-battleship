
package edu.duke.yz735.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * This class defineds BattleShipBoard
 * It has:
 * @field width: width of this Board
 * @field height: height of this Board
 * @field myShips: the ships on this Board
 * @field placementChecker: Rules of this Board
 */

public class BattleShipBoard<T> implements Board<T>{
  //fields
  private final int width;//width of this Board
  private final int height;//height of this Board
  private final ArrayList<Ship<T> > myShips;//the ships on this Board
  private final PlacementRuleChecker<T> placementChecker;//Rules of this Boar
  HashSet<Coordinate> enemyMisses;//record the Coordinates where enemy fires missed
  HashMap<Coordinate, T> enemyHits;//record the Coordinates and the informations at where enemy fires hitted
  private int nMoves;//number of moves available (3 to begin with)
  private int nSonar;//number of sonar available (3 to begin with)
  final T missInfo;//what to display for misses
  
  //constructors
  public BattleShipBoard (int w, int h, InBoundsRuleChecker<T> checker, T missInfo){
    /**
     * Constructs a BattleShipBoard with the specified width and height
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @param checker is the Rule checker of ship placements
     * @param missInfo is the data to display for missed shots
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    if (w <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
    }
    if (h <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
    }

    this.width = w;
    this.height = h;
    this.myShips = new ArrayList<>();
    this.placementChecker = checker;
    this.enemyMisses = new HashSet<Coordinate>();
    this.enemyHits = new HashMap<Coordinate, T>();
    this.missInfo = missInfo;
    this.nMoves = 3;//3 to begin with
    this.nSonar = 3;//3 to begin with
  }

  public BattleShipBoard(int w, int h, T missInfo) {
    this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)), missInfo);
  }

  //methods
  public int getWidth() {
    /**
     * @returns the width of the BattleShipBoard
     */
    return width;
  }

  public int getHeight() {
    /**
     * @returns the height of the BattleShipBoard
     */
    return height;
  }

  public int movesRemaining() {
    /**
     * @returns the number of moves remaining for this BattleShipBoard
     */  
    return nMoves;
  }

  public int sonarsRemaining() {
    /**
     * @returns the ship that contains Coordinate where
     */
    return nSonar;
  }

  public Ship<T> selectShip(Coordinate where){
    /**
     * @returns the ship that contains Coordinate where
     */
 
    //if the coordinate is occupied by one of my ships
    for (Ship<T> s: myShips) {
      if (s.occupiesCoordinates(where)){
        //return the displayInfo at that coordinate
        return s;
      }
    }
    return null;
  }

  public String sonarScan(Coordinate center){
    /**
    * considers the following pattern
     around (and including) the center (C)

        *
       ***
      *****
     ***C***
      *****
       ***
        *

     @returns on the number of squares occupied by each
     * type of ship in that region:
     *     Submarines occupy 2 squares
     *     Destroyers occupy 0 squares
     *     Battleships occupy 5 squares
     *     Carriers occupy 1 square  
     */
    HashSet<Coordinate> scanRegion = getScanRegion(center);

    HashMap<String, Integer> map = doScan(scanRegion);

    nSonar--;
    
    return printScan(map);
  }

  public String printScan(HashMap<String, Integer> map){
    /**
     * get the string output of the scanning results
     */
    StringBuilder sb = new StringBuilder();
    sb.append("Submarines occupy "+map.get("Submarine")+" squares\n");
    sb.append("Destroyers occupy "+map.get("Destroyer")+" squares\n");
    sb.append("Battleships occupy "+map.get("Battleship")+" squares\n");
    sb.append("Carriers occupy "+map.get("Carrier")+" squares\n");

    return sb.toString();
  }
  
  public HashSet<Coordinate> getScanRegion(Coordinate center) {
    /**
    * get set of coordinates for the following pattern
     around (and including) the center (C)

        *
       ***
      *****
     ***C***
      *****
       ***
        *

    */
    HashSet<Coordinate> scanRegion = new HashSet<Coordinate>();
    int center_row = center.getRow();
    int center_col = center.getCol();
    for (int i = 0; i <= 3; i++){
      for(int j = 0; j <= 3-i; j++){
        scanRegion.add(new Coordinate(center_row+i, center_col+j));
        scanRegion.add(new Coordinate(center_row+i, center_col-j));
        scanRegion.add(new Coordinate(center_row-i, center_col+j));
        scanRegion.add(new Coordinate(center_row-i, center_col-j));
      }
    }
    return scanRegion;
  } 
  
  public HashMap<String, Integer> doScan (HashSet<Coordinate> scanRegion){
    /**
     * construct the mapping for ship names and number of squares they occupy
     * @param HashSet<Coordinate> scanRegion
     * @return HashMap<String, int>: which maps
     * ship names to the number of squares they occupy
    */

    HashMap<String, Integer> ans = new HashMap<String, Integer>();
    ans.put("Submarine",0);
    ans.put("Destroyer",0);
    ans.put("Battleship",0);
    ans.put("Carrier",0);
    
    for (Coordinate c: scanRegion){
      Ship<T> s = selectShip(c);
      if (s!=null){
        ans.put(s.getName(), ans.get(s.getName())+1);
      }
    }
    
    return ans;
    
  }
  
  public String tryMoveTo(Ship<T> oldShip, Ship<T> newShip){
    /**
     * Move the selected ship to another placement
     * @param oldShip
     * @param newShip
     * @returns check_message if success placement
     * @returns null if fail placement
     * nMoves - 1 if success move
     */
    if (oldShip.getName()!= newShip.getName()){
      throw new IllegalArgumentException("You are moving a ship!");
    }
    myShips.remove(oldShip);//remove old ship and check placement for newship
    String check_message = placementChecker.checkPlacement(newShip, this);
    
    //if check message is null, means rules passed, and need nMoves > 0 to perform move
    if (check_message==null && nMoves>0){
      int index = 0;
      myShips.add(newShip);
      for (Coordinate old_c : oldShip.getCoordinates()){
        Coordinate new_c = newShip.getCoordinates().get(index);
        if (oldShip.wasHitAt(old_c)){
          newShip.recordHitAt(new_c);
        }
        index++;
      }
      nMoves--;//reduce available moves
    }
    //if cannot move, add old ship back to ship list
    else {
      myShips.add(oldShip);
    }
    return check_message;
  }

  
  public String tryAddShip(Ship<T> toAdd){
    /**
     * Adds a ship into myShips
     * @param toAdd is the ship to be added to the Board.
     * @returns null if satisfies placement rulse; specific String error message otherwise.
     */
    String check_message = placementChecker.checkPlacement(toAdd, this);
    if (check_message==null){
      myShips.add(toAdd);
    }
    return check_message;
  }
  
  public T whatIsAtForSelf(Coordinate where) {
    /** convenient function to return the display info at where for my ship
     */
    return whatIsAt(where, true);
  }

  public T whatIsAtForEnemy(Coordinate where) {
    /** convenient function to return the display info at where for enemy ship
     */
    return whatIsAt(where, false);
  }
  
  protected T whatIsAt(Coordinate where, boolean isSelf){
    /** This method takes a Coordinate, and sees 
     * which (if any) Ship occupies that coordinate
     * @param where is the target coordinate
     * @param boolean isSelf: whether is my ship or enemy ship
     * @returns displayInfo of the ship at coordinate.  
     * @returns null for myself if nothing at this coordinate 
     * @returns missInfo for enemy if nothing at this coordinate
     */
    //if for my own board
    if(isSelf){
      //select the ship at where
      Ship<T> s = selectShip(where);
      if (s!=null){
        return s.getDisplayInfoAt(where,true);
      }
    }

    else{
      if(enemyMisses.contains(where)){
        return missInfo;
      }
      return enemyHits.get(where);
    }
    return null;
  }
  
  public Ship<T> fireAt(Coordinate c){
    /**
     * This method should search for any ship that occupies 
     * @param coordinate c
     * If one is found, record the hit for that ship and 
     * @retrun this ship
     * If no ships are at this coordinate, record
     * the miss in the enemyMisses HashSet that we just made, and 
     * @return null.
     */
    //if ship found, record hit 
    Ship<T> s = selectShip(c);
    if (s!=null){
      s.recordHitAt(c);
      enemyHits.put(c, s.getDisplayInfoAt(c, false));//record hit for this board
      enemyMisses.remove(c);//remove miss on this coordinate if missed before
    }
    else{
      //if no ship found, record misses
      enemyMisses.add(c);
    }
    return s;
  }

  public boolean firedBefore(Coordinate c){
    /**
     * @returns true if c has been choose as a location to fire
     * @returns false if not
     */
    return enemyMisses.contains(c) || enemyHits.keySet().contains(c);
  }

  public boolean checkAllSunk(){
    /**
     * This method checks if all ships on this board has sunk
     * @return true if so;
     * @return false if not;
     */
    for (Ship<T> s : myShips){
      if (!s.isSunk()){
        return false;
      }
    }
    return true;
  }
  
}

