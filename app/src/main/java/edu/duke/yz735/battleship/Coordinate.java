package edu.duke.yz735.battleship;
/**
 * This class defineds Coordinate fields (row, column)
 * and methods (getRow(), getCol())
 * the range of row and column (not larger than the Board)
 * is enforced by Coordinate constructor
 */

public class Coordinate {
  private final int row;

  public int getRow() {
    return row;
  }

  private final int col;

  public int getCol() {
    return col;
  }

  public Coordinate (int r, int c){
    /**
     * Constructs a Coordinate with the specified row and column
     * @param r is the row of the newly constructed Coordinate.
     * @param c is the column of the newly constructed Coordinate.
     */
    this.row = r;
    this.col = c;
  }

  public Coordinate(String descr){
    /**
     * Constructs a Coordinate with the input String
     * @param descr is the input String with length 2 
     * first character of descr is the row as represented
     * in a single English character in Alphabetical order
     * second character of descr is the col as represented in a one digit number
     * (e.g. A2 (row=0, column =2))
     * @throws IllegalArgumentException if the row or column are not within valid range.
     */
    if (descr.length()!=2){
      throw new IllegalArgumentException("Input Coordinate String must be in format \"(One English letter, One digit number)\" ");
    }

    String up_descr = descr.toUpperCase();
    int r = (int)(up_descr.charAt(0) - 'A');
    int c = (int)(up_descr.charAt(1) - '0');

    this.row = r;
    this.col = c;
  }

  @Override
  public boolean equals(Object o) {
    /**
     * Override the equals method
     * compares two Coordinates
     * @return true if two Coordinates have same row and column
     * @return false if two Coordinates have different row or column
     */
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && col == c.col;
    }
    return false;
  }

  @Override
  public String toString() {
    /**
     * Override the toString() method
     * @return (row, column)
     */
    StringBuilder sb = new StringBuilder();
    sb.append((char)('A'+row));
    sb.append((char)('0'+col));
    return sb.toString();
  }
  @Override
  public int hashCode() {
    /**
     * Override the hashCode() method
     * using String.hashCode() to hashcode return value of overrided toString()
     */
    return toString().hashCode();
  }
  
}
