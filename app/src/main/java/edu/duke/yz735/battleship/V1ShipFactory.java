package edu.duke.yz735.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {

  protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
    /**
     * @param where is the Placement of the new ship
     * @param w, h are the width and height of the new ship (assuming a vertical orientation)
     * @param letter is the display info of the ship to put in myData
     * @param name is the name of the ship
     * @returns the new ship we create
     */
    if (where.getOrientation() != 'H' && where.getOrientation() != 'V') {
      throw new IllegalArgumentException("Invalid Input Orientation: must be horizontal(H) or vertical(V)!");
    }
    int width = w;
    int height = h;
    if (where.getOrientation() == 'H'){
      width = h;
      height = w;
    }
    
    Ship<Character> newShip = new RectangleShip<Character>(name, where.getWhere(), width, height, letter, '*'); 
    return newShip;
  }
  
  @Override
  public Ship<Character> makeSubmarine(Placement where) {
    // TODO Auto-generated method stub
    return createShip(where, 1, 2, 's', "Submarine");
  }

  @Override
  public Ship<Character> makeBattleship(Placement where) {
    // TODO Auto-generated method stub
    return createShip(where, 1, 4, 'b', "Battleship");
  }
  
  @Override
  public Ship<Character> makeCarrier(Placement where) {
    // TODO Auto-generated method stub
    return createShip(where, 1, 6, 'c', "Carrier");   
  }

  @Override
  public Ship<Character> makeDestroyer(Placement where) {
    // TODO Auto-generated method stub
    return createShip(where, 1, 3, 'd', "Destroyer");
  }
  
}
