package edu.duke.yz735.battleship;

/** Interface for ShipDisplayInfo
 */
public interface ShipDisplayInfo<T> {
  public T getInfo(Coordinate where, boolean hit);
}
