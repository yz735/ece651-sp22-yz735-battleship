package edu.duke.yz735.battleship;

import java.util.HashMap;
import java.util.HashSet;

/* Interface for BattleShipBoard
 */
public interface Board<T> {
  /**
   * @returns the width of the BattleShipBoard
   */  
  public int getWidth();
  
  /**
   * @returns the height of the BattleShipBoard
   */  
  public int getHeight();

  /**
   * @returns the number of moves remaining for this BattleShipBoard
   */  
  public int movesRemaining();
  
  /**
   * @returns the number of sonars remaining for this BattleShipBoard
   */  
  public int sonarsRemaining();

  
  /**
   * @returns the ship that contains Coordinate where
   */
  public Ship<T> selectShip(Coordinate where);

  /**
   * considers the following pattern
   around (and including) the center (C)

       *
      ***
     *****
    ***C***
     *****
      ***
       *
   

   @returns on the number of squares occupied by each
   * type of ship in that region:
   *     Submarines occupy 2 squares
   *     Destroyers occupy 0 squares
   *     Battleships occupy 5 squares
   *     Carriers occupy 1 square  
   */
  public String sonarScan(Coordinate center);

  /**
   * get the string output of the scanning results
   */
  public String printScan(HashMap<String, Integer> map);

  /**
   * get set of coordinates for the following pattern
   around (and including) the center (C)

      *
     ***
    *****
   ***C***
    *****
     ***
      *

   */
  public HashSet<Coordinate> getScanRegion(Coordinate center);
  
  /**
   * construct the mapping for ship names and number of squares they occupy
   * @param HashSet<Coordinate> scanRegion
   * @return HashMap<String, int>: which maps
   * ship names to the number of squares they occupy
   */
  public HashMap<String, Integer> doScan (HashSet<Coordinate> scanRegion);
  
  
  /**
   * Adds a ship into myShips
   * @param toAdd is the ship to be added to the Board.
   * @returns null if satisfies placement rulse; specific String error message otherwise.
   */  
  public String tryAddShip(Ship<T> toAdd);

  /**
   * Move the selected ship to another placement
   * @param oldShip
   * @param newShip
   * @returns check_message if success placement
   * @returns null if fail placement
   * nMoves - 1 if success move
   */
  public String tryMoveTo(Ship<T> oldShip, Ship<T> newShip);
  
  /** This method takes a
   * @param where: the target coordinate
   * @returns displayInfo of my ship at coordinate.  
   * @returns null If none is found
   */
  public T whatIsAtForSelf(Coordinate where);

  /** This method takes a
   * @param where: the target coordinate
   * @return displayInfo of enemy ship at coordinate.  
   * @returns null If none is found
   */
  public T whatIsAtForEnemy(Coordinate where);
  
  /**
   * This method should search for any ship that occupies 
   * @param coordinate c
   * If one is found, record the hit for that ship and 
   * @retrun this ship
   * If no ships are at this coordinate, record
   * the miss in the enemyMisses HashSet that we just made, and 
   * @return null.
   */
  public Ship<T> fireAt(Coordinate c);

  /**
   * @returns true if c has been choose as a location to fire
   * @returns false if not
   */
  public boolean firedBefore(Coordinate c);
    
  
  /**
   * This method checks if all ships on this board has sunk
   * @return true if so;
   * @return false if not;
   */
  public boolean checkAllSunk();
    
}
