package edu.duke.yz735.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 */
public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;
  
  public BoardTextView(Board<Character> toDisplay) {
    /**
   * Constructs a BoardView, given the board it will display.
   * @param toDisplay is the Board to display
   */
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
  }

  public String displayMyOwnBoard() {    
    /**
     * This displays my Board,
     *e.g.
     *  0|1|2|3
     *A  |s| |  A
     *B  | | |  B
     *  0|1|2|3 
     * @return the String that is the text output for the given board
     */
    return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
  }

  public String displayEnemyBoard() {
    /**
     * This displays enemy Board,
     *e.g.
     *  0|1|2|3
     *A  |s| |X A
     *B  |X| |  B
     *  0|1|2|3 
     * @return the String that is the text output for the given board
     */  
    return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
  }
  
  protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {    
    /**
     * This is the body of displayMyOwnBoard and displayEnemyBoard
     * @param getSquareFn: the function to return the display info at a coordinate
     */
    StringBuilder ans = new StringBuilder("");
    ans.append(makeHeader());
    for (int i = 0; i < toDisplay.getHeight(); i++) {
      ans.append(makeOneRow(i,getSquareFn));
    }
    ans.append(makeHeader());
    return ans.toString(); 
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    /** 
     * This method displays both my board and enemy's board in one player's view
     * @param enemyView: the BoartTextView of the enemy: enemyView.displayEnemyBoard()
     * @param myHeader: input header string for this player
     * @param enemyHeader: input header string for enemy player
     * @return the String display of myBoard next to enemyBoard
     */
    int W = toDisplay.getWidth();
    String ans = "";
    
    StringBuilder headBuilder = new StringBuilder();
    
    headBuilder.append(buildBothBoardHelper("", myHeader, 5));
    headBuilder.append(buildBothBoardHelper(headBuilder.toString(), enemyHeader+"\n", 2*W+22));
    ans+=headBuilder.toString();
    
    //read lines from both boards
    String[] myBoard_lines = displayMyOwnBoard().split("\n");
    String[] enemyBoard_lines = enemyView.displayEnemyBoard().split("\n");
    //iterate through lines;
    for (int i = 0; i<myBoard_lines.length; i++){
      StringBuilder rowBuilder = new StringBuilder();
      rowBuilder.append(buildBothBoardHelper("", myBoard_lines[i], 0));
      rowBuilder.append(buildBothBoardHelper(rowBuilder.toString(),
                                             enemyBoard_lines[i]+"\n", 2*W+19));
      ans+=rowBuilder.toString();
    }

    return ans;
  }
  
  //HELPER FUNCTIONS
    
  String makeHeader() {
    /**
     * This makes the header line, e.g.  0|1|2|3|4\n
     * 
     * @return the String that is the header line for the given board
     */
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
    String sep=""; //start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }


  String makeOneRow(int row_i, Function<Coordinate, Character> getSquareFn) {
    /**
     * This makes each row starting with a different English character in large case , e.g. A  |s|X|d|  B\n
     * @param row_i: index of the row need to be built
     * @param getSquareFn: the function to get the info at the square coordinate
     * @return the String that is each row for the given board
     */
    char c = (char)('A' + row_i);
    StringBuilder ans = new StringBuilder("");
    ans.append(c);
    ans.append(" ");
    String sep=""; //start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      Coordinate where = new Coordinate(row_i, i);
      ans.append(sep);
      if (getSquareFn.apply(where) == null){
        ans.append(" ");
      }
      else {
        ans.append(getSquareFn.apply(where));
      }
      sep = "|";
    }
    ans.append(" ");
    ans.append(c);
    ans.append("\n");
    
    return ans.toString();
  }

  String buildBothBoardHelper(String prefix, String content, int offset){
    /**
     * This method helps to insert content with specified offset into an array
     * @param prefix: the data before the content
     * @param content: the content need to insert
     * @param offset: the offset of the content in a row
     * @return the string of this row
     */
    StringBuilder ans = new StringBuilder();
    int n_space = offset-prefix.length();
    for (int i = 0; i<n_space; i++){
      ans.append(" ");
    }
    ans.append(content);
    
    return ans.toString();
  }
  
}
