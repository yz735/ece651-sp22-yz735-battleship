package edu.duke.yz735.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
  private final String name;
  
  public String getName() {
    return name;
  }
  
  static ArrayList<Coordinate> makeCoords(Placement p, int w, int h, HashSet<Integer> toRemove){
    /**
     * get the list of coordinate of this ship
     * @param Placement p
     * @param int w
     * @param int h
     * in order
     */
    ArrayList<Coordinate> ans = new ArrayList<Coordinate>();
    Coordinate pivot = getPivot(p,w,h);
    char Orientation = p.getOrientation();
    int p_row = pivot.getRow();
    int p_col = pivot.getCol();

    int index = 0;
    for (int i = 0; i < h; i++){
      for (int j = 0; j < w; j++){
        Coordinate toAdd = new Coordinate(p_row + i, p_col + j);
        if(Orientation == 'R'){
          toAdd = new Coordinate(p_row + j, p_col - i);
        }
        if(Orientation == 'D'){
          toAdd = new Coordinate(p_row - i, p_col - j);
        }
        if(Orientation == 'L'){
          toAdd = new Coordinate(p_row - j, p_col + i);
        }
        if (!toRemove.contains(index)){
          ans.add(toAdd);
        }
        index++;
      }
    }

    return ans;
  }

  static Coordinate getPivot(Placement p, int width, int height){
    /**
     * get the pivot coordinate of this 
     * @param Placement p
     * @param int width
     * @param int height
     * pivot defined as the position of the ship as upperLeft part of U or V
     */
    char Orientation = p.getOrientation();
    int rowOffset = 0;
    int colOffset = 0;
    Coordinate upperLeft = p.getWhere();
    if(Orientation == 'R'){
      colOffset = height-1;
    }
    if(Orientation == 'D'){
      rowOffset = height-1;
      colOffset = width-1;
    }
    if(Orientation == 'L'){
      rowOffset = width-1;
    }
    return new Coordinate(upperLeft.getRow()+rowOffset,
                          upperLeft.getCol()+colOffset);
  }

  public RectangleShip(String name, Placement p, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, HashSet<Integer> toRemove) {
    /**
     * All it does is make the right coordinate set, and pass them up to the BasicShip constructor.
     */
    super(makeCoords(p, width, height, toRemove), myDisplayInfo, enemyDisplayInfo);
    this.name = name;
  }

  /** Convenient Constructors of RectangleShip (they are really rectangular!)
   */
  public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
    /**
     *that is, we will tell the paraent constructor that 
     *for my own view display:
     *data if not hit
     *onHit if hit
     *for the enemy view:
     *nothing if not hit
     *data if hit
     */
    this(name, new Placement(upperLeft, 'h'), width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data), new HashSet<Integer>());
  }
  
  
  public RectangleShip(Coordinate upperLeft, T data, T onHit) {
    this("testShip", upperLeft, 1, 1, data, onHit);
  }

  /** Convenient Constructors of RectangleShip with designed shape
   */
  public RectangleShip(String name, Placement p, int width, int height, T data, T onHit, HashSet<Integer> toRemove ) {
    /**
     *that is, we will tell the paraent constructor that 
     *for my own view display:
     *data if not hit
     *onHit if hit
     *for the enemy view:
     *nothing if not hit
     *data if hit
    */
    this(name, p, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data), toRemove);
  }
}

