package edu.duke.yz735.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    /** check the placement of theShip onto theBoard is not out of Boundary
     * @param theShip: the ship we place
     * @param theBoard: the board we play on
     * @returns null if satisfied my rules; specific message if not
     */
    for (Coordinate c : theShip.getCoordinates()){
      if (c.getRow() >= theBoard.getHeight()){
        return "That placement is invalid: the ship goes off the bottom of the board.";
      }
      if (c.getCol() >= theBoard.getWidth()){
        return "That placement is invalid: the ship goes off the right of the board.";
      }
      if (c.getRow() < 0){
        return "That placement is invalid: the ship goes off the top of the board.";
      }
      if (c.getCol() < 0){
        return "That placement is invalid: the ship goes off the left of the board.";
      }
    }
    return null;
  }

  public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
  
}
