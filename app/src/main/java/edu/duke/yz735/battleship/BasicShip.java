package edu.duke.yz735.battleship;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represent the basic Ship in our Battleship game, which is a subtype of Ship
 */

public abstract class BasicShip<T> implements Ship<T> {
  protected ShipDisplayInfo<T> myDisplayInfo;
  /**
   * hold the display info of this ship if its my ship
   */
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  /**
   * hold the display info of this ship if its enemy ship
   */
  protected HashMap<Coordinate, Boolean> myPieces;
  /**
   * hold locations of any shape of ship
   * Specifically, if we have a coordinate c, and we look it up in the map:
   * if myPieces.get(c)  is null, c is not part of this Ship
   * if myPieces.get(c)  is false, c is part of this ship and has not been hit
   * if myPieces.get(c)  is true, c is part of this ship and has been hit
   */
  protected ArrayList<Coordinate> myPiecesInOrder;

  
  public ArrayList<Coordinate> getCoordinates(){
   /**
   * Get all of the Coordinates that this Ship occupies.
   * @return An Iterable with the coordinates that this Ship occupies
   */
    return myPiecesInOrder;
  }
  
  public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo){
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    myPieces = new HashMap<Coordinate, Boolean>();
    myPiecesInOrder = new ArrayList<Coordinate>();
    for (Coordinate c: where) {
      myPieces.put(c,false);
      myPiecesInOrder.add(c);
    }
  }
  
  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    /**
      * check if the Coordinate where is occupied by this ship.
      * @return true if Coordinate where is occupied by this ship. false if not.
      */
    return myPieces.containsKey(where);
  }

  @Override
  public boolean isSunk() {
    /**
     * Check if this ship has been hit in all of its locations meaning it has been
     * sunk.
     * @return true if this ship has been sunk, false otherwise.
     */
    for (Coordinate c : myPieces.keySet()){
      if (!myPieces.get(c)){
        return false;
      }
    }   
    return true;
  }

  protected void checkCoordinateInThisShip(Coordinate c){
    /**
     * helper function
     * @throw IllegalArgumentException if where is not part of the Ship
     */
    if (myPieces.get(c) == null){
      throw new IllegalArgumentException("Coordinate c is not part of this ship");
    }
  }
  
  
  @Override
  public void recordHitAt(Coordinate where) {
    /**
     * Make this ship record that it has been hit at the given coordinate. The
     * specified coordinate must be part of the ship.
     * 
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    checkCoordinateInThisShip(where);
    myPieces.put(where, true);
  }

  @Override
  public boolean wasHitAt(Coordinate where) {
    /**
     * Check if this ship was hit at the specified coordinates. The coordinates must
     * be part of this Ship.
     * 
     * @param where is the coordinates to check.
     * @return true if this ship as hit at the indicated coordinates, and false
     *         otherwise.
     * @throws IllegalArgumentException if the coordinates are not part of this
     *                                  ship.
     */
    checkCoordinateInThisShip(where);   
    return myPieces.get(where);
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myShip){
    /**
   * Return the view-specific information at the given coordinate. This coordinate
   * must be part of the ship.
   * 
   * @param where is the coordinate to return information for
   * @param myShip: true if its this player's ship, false otherwise
   * @throws IllegalArgumentException if where is not part of the Ship
   * @return The view-specific information at that coordinate.
   */
    checkCoordinateInThisShip(where);
    if (myShip){
      return myDisplayInfo.getInfo(where, myPieces.get(where));
    }
    else{
      return enemyDisplayInfo.getInfo(where, myPieces.get(where));
    }
  }
}
