package edu.duke.yz735.battleship;

public class Placement {
  /**
 * This class defineds Placement of a ship on the board with
 * Coordinate where (A location on the Board)
 * char orientation (Horizontal(H), Vertical(V)) (Case insensitive)
 */

  private final Coordinate where;

  public Coordinate getWhere() {
    return where;
  }
  
  private final char orientation;

  public char getOrientation() {
    return orientation;
  }

  public Placement (Coordinate w, char o){
    /**
     * Constructs a Placement with the specified Coordinate and orientation
     * @param w is the Coordinate.
     * @param o is the orientation.
     * @throws IllegalArgumentException if input orientation is not valid.
     */
    char up_o = Character.toUpperCase(o);
        
    if (up_o != 'H' && up_o != 'V' && up_o != 'U' && up_o != 'R' && up_o != 'D' && up_o != 'L') {
      throw new IllegalArgumentException("Invalid Input Orientation!");
    }
    this.where = w;
    this.orientation = up_o;
  }

  public Placement(String text_p){
    /**
     * Constructs a Placement with the input String
     * @param text_p is the input String with length 3 
     * text_p[0]: row as represented
     * in a single English character in Alphabetical order
     * text_p[1]: col as represented in a one digit number
     * text_p[2]: orientation as in either 'H', 'V', 'U', 'R', 'D', 'L'
     * (e.g. A2V (row=0, column =2, orientation = vertical))
     * @throws IllegalArgumentException if the row or column are not within valid range.
     */
    if (text_p.length()!=3){
      throw new IllegalArgumentException("Input Placement String must be in format \"(row index, column index, orientation)\"");
    }

    String up_text_p = text_p.toUpperCase();
    int r = (int)(up_text_p.charAt(0) - 'A');
    int c = (int)(up_text_p.charAt(1) - '0');

    Coordinate w = new Coordinate(r, c);
    char o = up_text_p.charAt(2);
    
    if (o != 'H' && o != 'V' && o != 'U' && o != 'R' && o != 'D' && o != 'L') {
      throw new IllegalArgumentException("Invalid Input Orientation!");
    }
    
    this.where = w;
    this.orientation = o;
  }

  @Override
  public boolean equals(Object o) {
    /**
     * Override the equals method
     * compares two Placements
     * @return true if two Placements have same Coordinate and orientation
     * @return false if two Coordinates have different Coordinate or orientation
     */
    if (o.getClass().equals(getClass())) {
      Placement p = (Placement) o;
      return where.equals(p.where) && orientation == p.orientation;
    }
    return false;
  }

  @Override
  public String toString() {
    /**
     * Override the toString() method
     * @return (where, orientaion)
     */
    return "("+where.toString()+", " + orientation+")";
  }
  @Override
  public int hashCode() {
    /**
     * Override the hashCode() method
     * using String.hashCode() to hashcode return value of overrided toString()
     */
    return toString().hashCode();
  }
}
