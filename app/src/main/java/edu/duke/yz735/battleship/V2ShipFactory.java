package edu.duke.yz735.battleship;

import java.util.HashSet;

public class V2ShipFactory extends V1ShipFactory{

  protected Ship<Character> createShapedShip(Placement where, int w, int h, char letter, String name){
    /**
     * @param where is the Placement of the new ship
     * @param w, h are the width and height of the new ship (assuming a vertical orientation)
     * @param letter is the display info of the ship to put in myData
     * @param name is the name of the ship
     * @returns the new ship we create
     */
    //throw exception when the orientation in Placement is not correct
    if (where.getOrientation() != 'U' && where.getOrientation() != 'R' &&
        where.getOrientation() != 'D' && where.getOrientation() != 'L') {
      throw new IllegalArgumentException("Invalid Input Orientation: must be up (U), right (R), down (D), or left (L)!");
    }

    //get width and height
    int width = w;
    int height = h;

    HashSet<Integer> toRemove = getToRemove(name);
    
    Ship<Character> newShip = new RectangleShip<Character>(name, where, width, height, letter, '*', toRemove);
    return newShip;
  }

  protected HashSet<Integer> getToRemove(String name){
    /** Helper function to get the pieces to remove on the ship
     * @param where: the Placement of the ship
     * @param name: the name of the ship (only taking Battleship and Carrier)
     * @return RemoveForBattleship(where) if for Battleship
     * @return RemoveForCarrier(where) if for Carrier
     */
    if (name.equals("Battleship")){
      /*
      * "Battleships" that are now shaped as shown below

              0b2     OR    b0        bbb        2b
              bbb           bb   OR   2b0    OR  bb
                            b2                   0b

               Up          Right      Down      Left
      */
      HashSet<Integer> ans = new HashSet<Integer>();
      ans.add(0);
      ans.add(2);
      return ans;
    }
    /** Helper function to the the piecese to remove from
     * "Carriers" that are now shaped as shown below
        
                  c1                      c8            
                  c3         8cccc        cc       13ccc
                  cc   OR    ccc31    OR  cc   OR  cccc8
                  cc                      3c         
                  8c                      1c
                   
                 Up           Right     Down          Left
    */
    HashSet<Integer> ans = new HashSet<Integer>();
    ans.add(1);
    ans.add(3);
    ans.add(8);
    return ans;
  }
  
  @Override
  public Ship<Character> makeBattleship(Placement where) {
    return createShapedShip(where, 3, 2, 'b', "Battleship");
  }

  
  @Override
  public Ship<Character> makeCarrier(Placement where) {
    return createShapedShip(where, 2, 5, 'c', "Carrier");   
  }

}

